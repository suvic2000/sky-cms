const isDropdown = (element) => element.hasAttribute('data-dropdown')
const selectDropdownButtons = () => document.querySelectorAll('[data-dropdown]')
const selectDropdown = (dropdownButton) =>
    document.getElementById(dropdownButton.getAttribute('data-dropdown'))
const triggerDropdown = (dropdown) => (dropdown.hidden = !dropdown.hidden)
const closeDropdown = (dropdown) => (dropdown.hidden = true)
const assignDropdownListener = (dropdownButton) =>
    dropdownButton.addEventListener('click', () =>
        triggerDropdown(selectDropdown(dropdownButton))
    )

selectDropdownButtons().forEach(assignDropdownListener)

// window.addEventListener('click', (event) => {
//     if (!isDropdown(event.target)) {
//         selectDropdownButtons().forEach((dropdownButton) =>
//             closeDropdown(selectDropdown(dropdownButton))
//         )
//     }
// })

var scrollToTopBtn = document.querySelector('.scrollToTopBtn')
var rootElement = document.documentElement

function handleScroll() {
    // Do something on scroll
    var scrollTotal = rootElement.scrollHeight - rootElement.clientHeight
    if (rootElement.scrollTop / scrollTotal > 0.8) {
        // Show button
        scrollToTopBtn.classList.add('showBtn')
    } else {
        // Hide button
        scrollToTopBtn.classList.remove('showBtn')
    }
}

function scrollToTop() {
    // Scroll to top logic
    rootElement.scrollTo({
        top: 0,
        behavior: 'smooth',
    })
}
scrollToTopBtn.addEventListener('click', scrollToTop)
document.addEventListener('scroll', handleScroll)