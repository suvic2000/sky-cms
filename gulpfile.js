const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const twig = require('gulp-twig');
const svgstore = require('gulp-svgstore');
const browsersync = require('browser-sync').create();
const fs = require('fs');
const del = require('del');
const data = require('gulp-data');

const paths = {
    out: './public/',
    twigData: './src/twig.json'
}

function clearTask(cb) {
    if (fs.existsSync(paths.out)) {
        del(paths.out + '*');
    }

    cb();
}

function htmlTask() {
    return src('./src/**/*.html')
        .pipe(dest(paths.out));
}

function twigTask() {
    return src('./src/**/[^_]*.twig')
        .pipe(data(function () {
            if (!fs.existsSync(paths.twigData)) {
                return {};
            }

            return JSON.parse(fs.readFileSync(paths.twigData));
        }))
        .pipe(twig())
        .pipe(dest(paths.out));
}

function imagesTask() {
    return src(['./src/imgs/**/*', '!**/*.svg'])
        .pipe(dest(paths.out + 'imgs'));
}

function scssTask() {
    return src('./src/styles/index.scss', { sourcemaps: true })
        .pipe(sass({
            includePaths: './src/styles'
        }))
        .pipe(dest(paths.out + '/styles', { sourcemaps: '.' }));
}

function jsTask() {
    return src('./src/**/*.js', { sourcemaps: true })
        .pipe(dest(paths.out, { sourcemaps: '.' }));
}

function svgTask() {
    return src('./src/imgs/icons/**/*.svg')
        .pipe(svgstore())
        .pipe(dest(paths.out + '/imgs'));
}

function browsersyncServe(cb) {
    browsersync.init({
        browser: 'chrome',
        server: {
            baseDir: paths.out,
        }
    });

    cb();
}

function browsersyncReload(cb) {
    browsersync.reload();

    cb();
}

function watchTask() {
    watch('src/**/*.html', series(htmlTask, browsersyncReload));
    watch(['src/**/*.scss'], series(scssTask, browsersyncReload));
    watch(['src/**/*.js'], series(jsTask, browsersyncReload));
    watch(['src/**/*.twig', paths.twigData], series(twigTask, browsersyncReload));
    watch(['src/imgs/**/*', '!**/*.svg'], series(imagesTask, browsersyncReload));
    watch(['src/imgs/icons/*'], series(svgTask, browsersyncReload));
}


exports.build = series(
    clearTask,
    htmlTask,
    twigTask,
    scssTask,
    jsTask,
    imagesTask,
    svgTask
);

exports.default = series(
    clearTask,
    htmlTask,
    twigTask,
    scssTask,
    jsTask,
    imagesTask,
    svgTask,
    browsersyncServe,
    watchTask
);
